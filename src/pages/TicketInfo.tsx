import React from "react";

import { useCurrentTicket } from "../hooks/useCurrentTicket";
import { Navigate } from "react-router-dom";
import { formatTicketData } from "../helpers/ticketHelpers";

import styles from "./TicketInfo.module.scss";
import { CompanyLogo } from "../components/CompanyLogo";
import { TransportIcon } from "../components/TransportIcon";

export default function TicketInfo() {
  // Get the current ticket from context
  const { currentTicket } = useCurrentTicket();
  const ticketData = formatTicketData(currentTicket);

  // Redirect to home page in case of missing ticket data.
  if (!ticketData) return <Navigate replace to="/" />;

  return (
    <div className={styles["ticket-info"]}>
      <div className={styles["ticket"]}>
        <div className={styles["date"]}>{ticketData.date}</div>
        {/* keep in mind, trip should be repeated for the number of trips on the ticket */}
        {ticketData.segments.map((segment, i) => (
          <div key={i} className={styles["trip"]}>
            <div className={styles["departure"]}>
              <p className={styles["time"]}>{segment.departure.time}</p>
              <div className={styles["location"]}>
                <p className={styles["city"]}>{segment.departure.city}</p>
                <p className={styles["station"]}>{segment.departure.station}</p>
              </div>
            </div>
            <div className={styles["transport-type"]}>
              <TransportIcon transportType={segment.transportType} />
              <CompanyLogo companySlug={segment.companySlug} />
            </div>
            <div className={styles["arrival"]}>
              <p className={styles["time"]}>{segment.arrival.time}</p>
              <div className={styles["location"]}>
                <p className={styles["city"]}>{segment.arrival.city}</p>
                <p className={styles["station"]}>{segment.arrival.station}</p>
              </div>
            </div>
          </div>
        ))}
        <div className={styles["price"]}>Total Price: {ticketData.price}</div>
      </div>
    </div>
  );
}
