import React, { createContext, useState } from "react";
import { Ticket, CurrentTicketContext } from "../types";

// Create context
export const ContextStore = createContext<CurrentTicketContext | null>(null);

export const StoreProvider = ({ children }: { children: JSX.Element }) => {
  const [currentTicket, setCurrentTicket] = useState<Ticket | undefined>();

  return <ContextStore.Provider value={{ currentTicket, setCurrentTicket }}>{children}</ContextStore.Provider>;
};
