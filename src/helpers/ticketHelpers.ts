import { Ticket, FormattedTicket } from "../types";

export const parseTime = (date: string): string => {
  const time = new Date(date).toLocaleTimeString("en-GB", {
    hour: "2-digit",
    minute: "2-digit",
  });
  return time;
};

export const parseDate = (argDate: string): string => {
  const date = new Date(argDate);
  return new Intl.DateTimeFormat("en-GB", { weekday: "short", month: "short", day: "2-digit" }).format(date);
};

export const formatTicketData = (ticket: Ticket | undefined): FormattedTicket | undefined => {
  if (!ticket) return undefined;
  const date = parseDate(ticket.segments[0].departure.time);
  const price = Math.round(ticket.price) + " €";

  const companySlugs = ticket.segments.map((segment) => segment.companySlug);
  const firstTrip = ticket.segments[0];
  const lastTrip = ticket.segments[ticket.segments.length - 1];
  const departureTime = parseTime(firstTrip.departure.time);
  const arrivalTime = parseTime(lastTrip.arrival.time);

  const segments = ticket.segments.map((segment) => ({
    transportType: segment.transportType,
    companySlug: segment.companySlug,
    departure: {
      city: segment.departure.city,
      station: segment.departure.station,
      time: parseTime(segment.departure.time),
    },
    arrival: {
      city: segment.arrival.city,
      station: segment.arrival.station,
      time: parseTime(segment.arrival.time),
    },
  }));

  return {
    companySlugs,
    firstTrip,
    lastTrip,
    departureTime,
    arrivalTime,
    date,
    price,
    segments,
  };
};
