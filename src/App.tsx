import React from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Header from "./components/Header/Header";
import styles from "./index.module.scss";
import Home from "./pages/Home";
import TicketInfo from "./pages/TicketInfo";
import { StoreProvider } from "./context/ContextStore";

function App() {
  return (
    <StoreProvider>
      <BrowserRouter>
        <Header />
        <div className={styles["kombo-content"]}>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/ticket" element={<TicketInfo />} />
          </Routes>
        </div>
      </BrowserRouter>
    </StoreProvider>
  );
}

export default App;
