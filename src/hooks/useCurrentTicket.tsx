import { useContext } from "react";
import { CurrentTicketContext } from "../types";
import { ContextStore } from "../context/ContextStore";

export const useCurrentTicket = () => useContext(ContextStore) as CurrentTicketContext;
