import React from "react";
import { Link } from "react-router-dom";
import { Ticket } from "../../types";
import { CompanyLogo } from "../CompanyLogo";
import { TransportIcon } from "../TransportIcon";
import styles from "./TicketResult.module.scss";
import { formatTicketData } from "../../helpers/ticketHelpers";
import { useCurrentTicket } from "../../hooks/useCurrentTicket";

export default function TicketResult({ ticket }: { ticket: Ticket }) {
  const { setCurrentTicket } = useCurrentTicket();

  const ticketData = formatTicketData(ticket);

  if (!ticketData) return null;

  // On click, we add the current ticket to the context store to show it on the /ticket page.
  // Clearly not perfect (reloads clear the context values),
  // but since tickets don't have IDs, that was the "best" solution I could come up with.
  return (
    <Link to="/ticket" onClick={() => setCurrentTicket(ticket)}>
      <div className={styles["ticket"]}>
        <div className={styles["header"]}>
          {/* we will always only display the first transport type */}
          <TransportIcon transportType={ticketData.firstTrip.transportType} />

          {/* we will always display all the companies */}
          {ticketData.companySlugs.map((slug, i) => (
            <div key={i} className={styles["company-logo"]}>
              <CompanyLogo companySlug={slug} />
            </div>
          ))}
        </div>

        <div className={styles["body"]}>
          <div className={styles["schedules"]}>
            <span className={styles["schedules-time"]}>{ticketData.departureTime}</span>
            <span className={styles["schedules-time"]}>{ticketData.arrivalTime}</span>
          </div>

          <div className={styles["locations"]}>
            <p className={styles["locations-line"]}>
              <span className={styles["city"]}>{ticketData.firstTrip.departure.city}</span>
              {ticketData.firstTrip.departure.station}
            </p>
            <p className={styles["locations-line"]}>
              <span className={styles["city"]}>{ticketData.lastTrip.arrival.city}</span>
              {ticketData.lastTrip.arrival.station}
            </p>
          </div>

          <div className={styles["price"]}>
            <span className={styles["amount"]}>{ticketData.price}</span>
          </div>
        </div>
      </div>
    </Link>
  );
}
